<?php

namespace Drupal\goaway\EventSubscriber;

use Drupal\Core\Routing\TrustedRedirectResponse;
use \Drupal\Core\Url;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GoAwaySubscriber implements EventSubscriberInterface {

  public function checkForRedirection(GetResponseEvent $event) {
    $ip = $event->getRequest()->getClientIp();

    $db = \Drupal::database();
    $banned = (bool) $db->select('goaway', 'ga')
      ->fields('ga', ['gid'])
      ->condition('ip', $ip)
      ->range(0, 1)
      ->execute()->fetchAssoc();

    if ($banned) {
      $jumpto = \Drupal::config('goaway.settings')->get('goaway_dest');
      $redirect_url = $event->getRequest()->server->get('REDIRECT_URL');
      $msg = t('YOU ARE BANNED.');
      drupal_set_message($msg);

      if(isset($redirect_url)){
        if(empty($jumpto)){
          $event->setResponse(new TrustedRedirectResponse(Url::fromRoute('<front>')->toString()));
        }
        elseif($redirect_url != $jumpto && $redirect_url != "/" . $jumpto){
          $event->setResponse(new TrustedRedirectResponse($jumpto));
        }
      }
      elseif(!empty($jumpto)){
        $event->setResponse(new TrustedRedirectResponse($jumpto));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

}