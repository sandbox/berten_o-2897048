<?php

namespace Drupal\goaway\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class GoAwaySettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'goaway_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'goaway.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
        $config = $this->config('goaway.settings');

        $form['goaway_dest'] = array(
            '#prefix' => t('Enter a destination for miscreants. If using a remote address, the full URL (including http://) must be used.	For a local page, use the relative Drupal path (eg. node/1). 
            If left empty, the user will be redirected to the front page.'),
            '#type' => 'textfield',
            '#title' => t('Redirect Destination'),
            '#default_value' => $config->get('goaway_dest'),
            '#size' => '60',
        );
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('goaway.settings')
            ->set('goaway_dest', $values['goaway_dest'])
            ->save();
    }

}