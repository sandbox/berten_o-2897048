<?php
/**
 * @file
 * Contains \Drupal\goaway\Form\GoAwayUnbanIPForm.
 */

namespace Drupal\goaway\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class GoAwayUnbanIPForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'goaway_unban_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $gid = NULL) {
    $form['goaway_unban'] = array(
      '#type' => 'hidden',
      '#value' => $gid,
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Unban'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gid=$form_state->getValue('goaway_unban');
    $ip = goaway_get_ip_from_ban($gid);

    $db=\Drupal::database();
    $db->delete('goaway')
      ->condition('ip', $ip)
      ->execute();

    $msg = 'Unbanned IP: ' . $ip;
    \Drupal::logger('goaway')->notice($msg);
    drupal_set_message(t($msg));
    $form_state->setRedirect('goaway.ban_ip_list');
  }
}