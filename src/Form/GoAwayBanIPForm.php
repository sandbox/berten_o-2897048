<?php
/**
 * @file
 * Contains \Drupal\goaway\Form\GoAwayBanIPForm.
 */

namespace Drupal\goaway\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class GoAwayBanIPForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'goaway_ban_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $ip = NULL) {
    $path = $this->getDestinationArray();
    $form['goaway_ban'] = array(
      '#type' => 'hidden',
      '#value' => $ip,
    );
    $form['came_from'] = array(
      '#type' => 'hidden',
      '#value' => $path,
    );
    	$form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Ban'),
    );
	$form['actions']['cancel'] = array(
		'#type' => 'submit',
		'#value' => t('Cancel'),
		'#submit' => array('::cancelForm')
	);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $banned_ip = $form_state->getValue('goaway_ban');
    $path = $form_state->getUserInput()['came_from'];
    $banned = goaway_check_for_ban($banned_ip);
    if ($banned) {
      $msg = 'IP: ' . $banned_ip . " is already banned.";
      drupal_set_message(t($msg));
    }
    else {
      $db = \Drupal::database();
      $ban_success = $db->insert('goaway')
        ->fields([
          'gid',
          'ip',
        ])
        ->values([
          0,
          $banned_ip,
        ])
        ->execute();

      if ($ban_success) {
        $msg = 'Banned IP: ' . $banned_ip;
        \Drupal::logger('goaway')->notice($msg);
        drupal_set_message(t($msg));
      }
      else {
        $msg = 'Failed to ban IP: ' . $banned_ip;
        \Drupal::logger('goaway')->error($msg);
        drupal_set_message(t($msg));
      }
    }
    $url = Url::fromUri('internal:' . $path);
    $form_state->setRedirectUrl($url);
  }

  public function cancelForm(array &$form, FormStateInterface $form_state) {
  $path = $form_state->getUserInput()['came_from'];
      $url = Url::fromUri('internal:' . $path);
      $form_state->setRedirectUrl($url);
  }
}