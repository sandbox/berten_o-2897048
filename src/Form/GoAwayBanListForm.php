<?php
/**
 * @file
 * Contains \Drupal\goaway\Form\GoAwayBanList.
 */

namespace Drupal\goaway\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class GoAwayBanListForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'goaway_banlist_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $ip = NULL) {
    $form['goaway_banlist'] = array(
      '#type' => 'textfield',
      '#title' => t('Ban New IP'),
      '#default_value' => '',
      '#size' => 18,
      '#maxlength' => 15,
      '#description' => t('Enter an IP Address for banning (e.g.: 123.123.123.123)'),
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add to List'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if($form_state->hasValue('goaway_banlist')){
      $goaway_banlist=$form_state->getValue('goaway_banlist');
      if (!preg_match( "/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/", $goaway_banlist)) {
        $form_state->setErrorByName('goaway_banlist',t('Data submitted not in IP format'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $banned_ip = $form_state->getValue('goaway_banlist');
    $banned = goaway_check_for_ban($banned_ip);
    if($banned) {
      $msg = 'IP: ' . $banned_ip . " is already banned.";
      drupal_set_message(t($msg));
    } else {
      $db = \Drupal::database();
      $ban_success = $db->insert('goaway')
        ->fields([
          'gid',
          'ip',
        ])
        ->values([
          0,
          $banned_ip,
        ])
        ->execute();
      if ($ban_success) {
        $msg = 'Banned IP: ' . $banned_ip;
        \Drupal::logger('goaway')->notice($msg);
        drupal_set_message(t($msg));;
      }
      else {
        $msg = 'Failed to ban IP: ' . $banned_ip;
        \Drupal::logger('goaway')->error($msg);
        drupal_set_message(t($msg));
      }
    }
    $form_state->setRedirect('goaway.ban_ip_list');
  }
}