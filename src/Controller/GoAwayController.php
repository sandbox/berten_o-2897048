<?php

namespace Drupal\goaway\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * GoAwayController
 */
class GoAwayController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function BanList() {
    $db = \Drupal::database();
    $result = $db->select('goaway', 'ga')
      ->fields('ga', array('gid', 'ip'))
      ->orderBy('gid', 'DESC')
      ->execute();

    $banned_ips = array();
    foreach ($result as $row) {
      $link = Link::createFromRoute('unban', 'goaway.unban_ip', ['gid' => $row->gid])
        ->toRenderable();
      $link = render($link);
      $banned_ips[] = array(
        '#type' => 'markup',
        '#markup' => $row->ip . ' - ' . $link,
      );
    }

    $page_content = \Drupal::formBuilder()
      ->getForm('Drupal\goaway\Form\GoAwayBanListForm');
    $build = array(
      'headline' => array(
        '#type' => 'markup',
        '#markup' => '<h2>' . t('Banned IPs:') . '</h2>',
      ),
      'body' => array(
        '#items' => $banned_ips,
        '#theme' => 'item_list',
      ),
      $page_content,
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function BanByIPPage($ip) {
    $page_content = '<p>' . $ip . '</p>';
    $banform = \Drupal::formBuilder()
      ->getForm('Drupal\goaway\Form\GoAwayBanIPForm', $ip);

    $build = array(
      'headline' => array(
        '#type' => 'markup',
        '#markup' => '<h2>' . t('Are you sure you want to ban this IP address?') . '</h2>',
      ),
      'body' => array(
        '#type' => 'markup',
        '#markup' => $page_content,
      ),
      $banform
    );
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function UnbanByIPPage($gid) {
    $page_content = '<p>' . goaway_get_ip_from_ban($gid) . '</p>';
    $unbanform = \Drupal::formBuilder()
      ->getForm('Drupal\goaway\Form\GoAwayUnbanIPForm', $gid);
    $build = array(
      'headline' => array(
        '#type' => 'markup',
        '#markup' => '<h2>' . t('Are you sure you want to unban this IP address?') . '</h2>',
      ),
      'body' => array(
        '#type' => 'markup',
        '#markup' => $page_content,
      ),
      $unbanform,
    );

    return $build;
  }

}

